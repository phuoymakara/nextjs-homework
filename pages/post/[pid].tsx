import { useRouter } from "next/router";
import posts from "../DataFile/data";
import { type } from "os";
import Navbartop from "../../src/components/Navbar/navbar";
import { CustomCard } from "../../src/components/CustomCard/Card";



export const Post = ( ) =>{
    const route = useRouter();
    const { pid } = route.query;

    const page = posts.filter( p => String(p.id) === String(pid));

    return(
        <>
            <Navbartop/>
            <h1 className="text-center text-info display-2 mt-0 p-0">Hi here's Post {pid}</h1>
            {
                page.map( (p) =>{
                    return(
                        <div className="p-4">
                            <CustomCard
                                id={p.id} 
                                title={p.title} 
                                description={p.description} 
                                image={p.urlToImage} 
                                author={p.author}
                                pagesise = "100%"
                                imgsize = "100%"
                            /  >
                        </div>
                    )
                })
            }
        </>
    )

}

export default Post;