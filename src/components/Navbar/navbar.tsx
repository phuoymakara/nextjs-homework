import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

export const Navbartop = () =>{
    const [collapsed, setCollapsed] = useState(true);

    const toggleNavbar = () => setCollapsed(!collapsed);

    return(
        <div className='bg-info '>
            <Navbar color="faded" light  className='sticky'>
                <NavbarBrand href="/" className="me-auto text-white ">
                <h1 className='p-0 m-0'>Reactstrap</h1>
                </NavbarBrand>
                <NavbarToggler onClick={toggleNavbar} className="me-2" />
                <Collapse isOpen={!collapsed} navbar>
                <Nav navbar >
                    <NavItem>
                    <NavLink href="/components/" className='text-white'>Components</NavLink>
                    </NavItem>
                    <NavItem>
                    <NavLink href="https://github.com/reactstrap/reactstrap" className='text-warning'>
                        GitHub
                    </NavLink>
                    </NavItem>
                </Nav>
                </Collapse>
            </Navbar>
        </div>
    )
}

export default Navbartop;