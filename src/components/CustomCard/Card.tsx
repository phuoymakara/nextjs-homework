import { useRouter } from "next/router"
import { Card, CardBody, CardTitle, CardSubtitle, CardText, Button } from "reactstrap";


type CardProps = {
    id          ?: number;
    title       ?: string;
    description ?: string;
    image       ?: string;
    author      ?: string;
    pagesise    ?: string;
    imgsize     ?: string;
}
export const CustomCard = ({ id, title, description , image, author, pagesise, imgsize } : CardProps ) =>{

    const route = useRouter();
   
    return (
            <Card style={{ width: pagesise , height: pagesise}} className="mb-4">
                <img alt="Sample" height={imgsize} src={`${image}`} />
                <CardBody>
                    <CardTitle tag="h5" className="line-header-title">
                        {title}
                    </CardTitle>
                    <CardSubtitle className="mb-2 text-muted" tag="h6">
                        {author}
                    </CardSubtitle>
                    <CardText className="line-claim">
                        {description}
                    </CardText>
                    <Button onClick={ () => route.push(`/post/${id}`)}> view </Button>
                </CardBody>
            </Card>        
    )
}